package lesson12;

import java.util.HashMap;
import java.util.Map;

public class Counter<A> {

    private final Map<A, Integer> data = new HashMap<>();

    public void count(A c) {
        data.merge(c, 1, (a, b) -> a + b);
    }

    public Map<A, Integer> get() {
        return data;
    }

}
