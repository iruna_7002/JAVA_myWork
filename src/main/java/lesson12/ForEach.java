package lesson12;

import java.util.Iterator;
import java.util.function.Consumer;

public interface ForEach<A> extends Iterable<A> {

    default void foreach(Consumer<A> consumer) {
        for (Iterator<A> it = this.iterator() ; it.hasNext() ;) {
            A a = it.next();
            consumer.accept(a);
        }
    }

    default void foreach2(Consumer<A> consumer) {
        for (A a: this) {
            consumer.accept(a);
        }
    }

}
