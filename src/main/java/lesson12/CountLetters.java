package lesson12;

import java.util.HashMap;
import java.util.Map;

public class CountLetters {

    static Map<Character, Integer> countLetters1(String line) {
        Map<Character, Integer> outcome = new HashMap<>();
        for (char c: line.toCharArray()) {
            if (!outcome.containsKey(c)) {
                outcome.put(c, 1);
            } else {
                int count = outcome.get(c);
                outcome.put(c, count + 1);
            }
        }
        return outcome;
    }

    static Map<Character, Integer> countLetters2(String line) {
        Map<Character, Integer> outcome = new HashMap<>();
        for (char c: line.toCharArray()) {
            int count = outcome.getOrDefault(c, 0);
            outcome.put(c, count + 1);
        }
        return outcome;
    }

    static Map<Character, Integer> countLetters(String line) {
        Map<Character, Integer> outcome = new HashMap<>();
        for (char c: line.toCharArray()) {
            outcome.merge(c, 1, (a, b) -> a + b);
        }
        return outcome;
    }

    static Map<Character, Integer> countLetters3(String line) {
        Counter<Character> counter = new Counter<>();
        for (char c: line.toCharArray()) {
            counter.count(c);
        }
        return counter.get();
    }

    public static void main(String[] args) {
        String line = "Hello World!";
        Map<Character, Integer> outcome = countLetters(line);
        System.out.println(outcome);
    }

}
