package lesson05;

public class Board {
    public static String representCell(int value) {
        if (value == 1) return "@";
        if (value == 2) return "X";
        return ".";
    }
    public static void printBoard(int[][] board) {
        System.out.printf("===========\n");
            for (int y=0; y < board.length; y++){
                for (int x = 0; x< board[y].length; x++) {
                    System.out.printf(representCell(board[y][x]));
                }
                System.out.println();
            }
        System.out.printf("===========\n");
    }

    public static void fillBoard(int[][] board) {
        board[7][1] = 1;
        board[8][1] = 1;
        board[9][1] = 1;
        board[5][6] = 2;

    }
    public static void main(String[] args) {
        int[][] board = new int[10][10]; // dvovimirn array
        //int[] row = board[0];  // first row
        fillBoard(board);
        printBoard(board);

    }
}
