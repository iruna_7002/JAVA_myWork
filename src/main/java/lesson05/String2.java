package lesson05;

import java.nio.charset.StandardCharsets;

public class String2 {
    public static void main(String[] args) {
        String s0 = "Hello, Java!";
        /////////////01234567891011
        String s1 = "hello";
        String s2 = Character.toUpperCase(s1.charAt(0)) + s1.substring(1);
        System.out.println(s2);

        String s3 = "алекс";
        byte[] bytes = s3.getBytes(StandardCharsets.UTF_8);
        System.out.println(bytes[0]);

        bytes[0] = (byte) Character.toUpperCase(bytes[0]);
        String s4 = new String(bytes);
        System.out.println(s4);

        char[] chars = new char[s4.length()];
        s4.getChars(0,5,chars, 0); // 5
        chars[0] = Character.toUpperCase(chars[0]);
        String s5 = new String(chars);
        System.out.println(s5);

        //char[] chars1 = s2.chars().to;
    }
}
