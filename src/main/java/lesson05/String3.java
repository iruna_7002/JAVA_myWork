package lesson05;

public class String3 {
    public static void main(String[] args) {
        String s1 = "Jim";
        System.out.println(s1.getBytes().length); // 3
        String s2 = "Алекс";
        System.out.println(s2.getBytes().length); // 10
        String s3 = s1 + s2;
        System.out.println(s3.getBytes().length); // 16(JDK8)

        char c = '\u7684';
        System.out.println(c);
    }
}
