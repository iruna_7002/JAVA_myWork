package lesson05;

import java.util.Arrays;

public class ArrayApp {
    public static void iterationWay1(int[] data) {
        int length = data.length; //10 | 5
        // index: 0....len-1
        for (int i = 0; i < length; i++){
            int x = data[i];
            System.out.printf("%d", x);
        }
    }
    public static void main(String[] args) {
        /// create array
        int[] ints = new int[10]; //array size 10
        int[] ints2 = {1,6,4,8,9}; // length - 5
        int size = ints.length; // 10
        int size2 = ints2.length; // 5
        iterationWay1(ints);
        iterationWay1(ints2);

        Arrays.sort(ints2);
        System.out.println(Arrays.toString(ints2));
        int[] ints11 = new int[ints2.length];
        for (int i = 0; i<ints2.length; i++){
            ints11[i] = ints2[ints2.length - i - 1];
        }
        System.out.println(Arrays.toString(ints11));
    }
}
