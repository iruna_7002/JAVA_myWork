package lesson04;

import java.util.Scanner;

public class IfElse5 {
    public static String doProcess(int x) {
        String message;
        if(x<10) {
            message = String.format("%d is less than 10", x);
        } else {
            message = String.format("%d is greater or equel than 10", x);
        }
        return message;
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();

        String message = doProcess(x);
        System.out.println(message);
    }
}

