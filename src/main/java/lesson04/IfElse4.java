package lesson04;

import java.util.Scanner;

public class IfElse4 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        String message;
         // x==3 - history
        // x == 10 - history
        //x == 50 - history
        // ...... etc
        switch (x){
            case 3 : message = "three";
            break;
            case 10 :
            case 11 : message = "ten";; // abo 10 abo 11  "ten or eleven"
            break;
            case 50 : message = "fifty";
            break;
            default: message =String.format("else: %d]n", x);
        }
        System.out.println(message);
        /* String message = switch (x){
            case 3 -> "three";
            case 10,
            case 11 -> "ten"; // abo 10 abo 11  "ten or eleven"
            case 50 -> "fifty";
           //vche nema break;
            default -> String.format("else: %d]n", x);
        }*/
    }
}
