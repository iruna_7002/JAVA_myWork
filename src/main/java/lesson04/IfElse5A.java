package lesson04;

import java.util.Scanner;

public class IfElse5A {
    public static String doProcess(int x) {
        String message = x < 10 ?
                String.format("%d is less than 10", x) :
                String.format("%d is greater or equel than 10", x) ;
        return message;
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();

        String message = doProcess(x);
        System.out.println(message);
    }
}

