package lesson04;

import java.util.Scanner;

import static labs.StringUtilits.isInt;
import static labs.StringUtilits.isLong;
import static labs.StringUtilits.isDouble;

public class IfElse3A {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String line = s.nextLine();
        String message;
        if(isInt(line)) { // potribno zi skobkamu bo tak tilki odun ryadok
            message = "Integer was entered";
        } else if(isLong(line)) { // potribno zi skobkamu bo tak tilki odun ryadok
            message = "Long was entered";
        } else if(isDouble(line)) { // potribno zi skobkamu bo tak tilki odun ryadok
            message = "Double was entered";
        } else {
            message = "non-Integer (String) was entered";
        }
        System.out.println(message);
    }
}