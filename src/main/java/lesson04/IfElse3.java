package lesson04;

import java.util.Scanner;

import static labs.StringUtilits.isInt;
import static labs.StringUtilits.isLong;
import static labs.StringUtilits.isDouble;

public class IfElse3 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String line = s.nextLine();
        if(isInt(line)) { // potribno zi skobkamu bo tak tilki odun ryadok
            System.out.println("Integer was entered");
        } else if(isLong(line)) { // potribno zi skobkamu bo tak tilki odun ryadok
            System.out.println("Long was entered");
        } else if(isDouble(line)) { // potribno zi skobkamu bo tak tilki odun ryadok
            System.out.println("Double was entered");
        } else {
            System.out.println("non-Integer (String) was entered");
        }
        System.out.println("going further");
    }
}
