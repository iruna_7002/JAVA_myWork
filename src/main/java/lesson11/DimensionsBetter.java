package lesson11;

import java.time.DayOfWeek;

public class DimensionsBetter {

    record Entry(DayOfWeek day, String status) {}

    static void fill(Entry[] data) {
        data[0] = new Entry(DayOfWeek.MONDAY, "free");
        data[1] = new Entry(DayOfWeek.MONDAY, "busy");
    }

    public static void main(String[] args) {
        Entry[] data = new Entry[10];
        fill(data);

        String status = data[0].status;
        DayOfWeek day = data[0].day;
        System.out.printf("status %s\n", status);
        System.out.printf("day %s\n", day);

    }

}
