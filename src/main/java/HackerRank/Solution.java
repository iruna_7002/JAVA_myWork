package HackerRank;
import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

    public class Solution {

        private static final Scanner scanner = new Scanner(System.in);

        public static void main(String[] args) {
            int N = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
            if (N % 2 != 0) {
                labs.Output.print("Weird");
            }
            if (N % 2 == 0 && 2<=N && N<=5) {
                labs.Output.print("Not Weird");
            }
            if (N % 2 == 0 && 6<=N && N<=20) {
                labs.Output.print("Weird");
            }
            if (N % 2 == 0 && 20<N) {
                labs.Output.print("Not Weird");
            }
            scanner.close();
        }
    }


