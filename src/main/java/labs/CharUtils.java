package labs;

public class CharUtils {
    //nichogo ne povertaye tome vidilyaetsya chervonim posle skobki
    public static boolean isLetter(char c){
        return isLower(c) || isUpper(c);//throw EX.NI;
    }
    public static boolean isNumber(char c){ // throw EX.NI
        return c>='0' && c<='9'; // all boolean
    }
    public static boolean isLower(char c){
        return c>= 'a' && c <= 'z';
    }
    public static boolean isUpper(char c){
        return c>= 'A' && c <= 'Z';
    }
    public static boolean isAlphaNumeric(char c){
        return isLetter(c) || isNumber(c);
    }
}
