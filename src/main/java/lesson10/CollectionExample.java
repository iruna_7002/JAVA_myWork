package lesson10;
import java.util.ArrayList;
import java.util.HashSet;

public class CollectionExample {
    public static void main(String[] args) {
        ArrayList<Integer> a1 = new ArrayList<Integer>();
        a1.add(11);
        a1.add(12);
        a1.add(12);
        a1.add(14);
        a1.add(15);

        System.out.println(a1);

        HashSet<String> set = new HashSet<>();
        set.add("Java");
        set.add("Scala");
        set.add("Python");
        set.add("JavaScript");

        System.out.println(set);
        HashSet<Integer> set2 = new HashSet<>(a1);
        System.out.println(set2);
    }
}
