package lesson10;

public class Inheritance {

    static class Animal{
        public  void go() {}
    }
    static class Dog extends Animal{
        public  void bark() {}
    }
    static class Cat extends Animal{
        public  void meow() {}
    }

    public static void main(String[] args) {
        Animal a = new Animal();
        Dog d1 = new Dog();
        Animal d2= new Dog();
        Object d3 = new Dog(); // не маємо доступ до методів тому що Об'єкт
        var d4 = new Dog();

        // left side
        a.go();
        d1.go();
        d2.go();
        // ні, бо д3 це об'єкт d3.go();
        d4.go();
        ((Dog)d2).bark();

        // right side
        System.out.println(a instanceof Animal);  // true
        System.out.println(d1 instanceof Animal); //true
        System.out.println(d2 instanceof Animal); //true
        System.out.println(d1 instanceof Object); // true
        System.out.println(d2 instanceof Object); // true
    }
}
