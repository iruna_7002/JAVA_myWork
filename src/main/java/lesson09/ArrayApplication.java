package lesson09;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArrayApplication {

    public static void whatever(Array a) {}

    public static void main(String[] args) {

        List<Integer> l1 = new ArrayList<>();
        List<Integer> l2 = new LinkedList<>();

        Array a1 = new ArrayInDatabase();
        Array a2 = new ArrayViaNetwork();
        Array a = new ArrayInMemory();

        whatever(a);
        whatever(a1);
        whatever(a2);
//        new ArrayInMemory(100);
//        new ArrayInMemory(2.0);
//        new ArrayInMemory(1000, 1.1);
        System.out.println(a);          // []
        a.add(11);
        a.add(21);
        a.add(31);
        a.add(99);
        a.add(55);
//        a.removeAt(2);
        System.out.println(a);          // [0, 11, 21]
//        a.add(7);
//        System.out.println(a);          // [7]
//        a.add(5);
//        System.out.println(a);          // [7, 5]
//        System.out.println(a.length()); // 2
//        a.removeLast();
//        System.out.println(a.length()); // 1
//        System.out.println(a.get(0));   // 7
//        System.out.println(a.get(1));   // Exception
//        a.add(10);
//        a.add(11);
//        a.removeAt(0);              // [10, 11]

    }

}
