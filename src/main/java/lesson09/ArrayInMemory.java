package lesson09;

import static labs.EX.NI;

public class ArrayInMemory implements Array {

    private int[] data;
    private int next;
    private final double growFactor;

    private final static int DEFAULT_SIZE = 16;
    private final static double DEFAULT_GROW_FACTOR = 1.5;

    public ArrayInMemory(int size, double growFactor) {
        this.data = new int[size];
        this.growFactor = growFactor;
        this.next = 0;
    }

    public ArrayInMemory(int size) {
        this(size, ArrayInMemory.DEFAULT_GROW_FACTOR);
    }

    public ArrayInMemory(double growFactor) {
        this(ArrayInMemory.DEFAULT_SIZE, growFactor);
    }

    public ArrayInMemory() {
        this(ArrayInMemory.DEFAULT_SIZE);
    }

    @Override
    public int length() {
        return next;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < next; i++) {
            if (i > 0) sb.append(", ");
            sb.append(data[i]);
        }
        sb.append("]");
        return sb.toString();
    }

    private int calcNewSize() {
        return (int)(data.length * growFactor + 1);
    }

    private void grow() {
        int[] data2 = new int[calcNewSize()];
        System.arraycopy(data, 0, data2, 0, data.length);
        data = data2;
    }

    private void ensureSizeEnough() {
        if (next >= data.length) grow();
    }

    @Override
    public void add(int x) {
        ensureSizeEnough();
        data[next] = x;
        next = next + 1;
    }

    @Override
    public int get(int idx) {
        if (idx >= next) throw new IndexOutOfBoundsException();
        return data[idx];
    }

    @Override
    public void removeLast() {
        if (next == 0) throw new IllegalStateException("Array is empty");
        next = next - 1;
        shrinkIfNeed();
    }

    @Override
    public void removeAt(int idx) {
        if (idx < 0) throw new IllegalStateException("index must be positive");
        if (idx > next) throw new IllegalStateException("index must be less than length");
        System.arraycopy(
                data, idx+1,
                data, idx,
                data.length - idx - 1
        );
        next = next - 1;
        shrinkIfNeed();
    }

    private void shrinkIfNeed() {
        // TODO: homework
    }

    @Override
    public void addAt(int x, int idx) {
        throw NI;
    }

}
