package lesson07;

import java.util.Arrays;

public class Box {
    static int x;
    public  final int[] data;

    public  Box(int[] data){
        this.data = data;
    }

    @Override
    public String toString() {
        return "Box{" +
                "data=" + Arrays.toString(data) +
                '}';
    }
}
