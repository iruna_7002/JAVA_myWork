package lesson07;

public class Restaurant2 {

    public static String makeName(){
        return "Marga" + "RITA".toLowerCase();
    }
    public static void main(String[] args) {
        Pizza p1 = new Pizza(makeName(), 60);
        Pizza p2 = new Pizza(makeName(), 60);
        Pizza p3 = new Pizza("Margaritha", 60);
        Pizza p4 = new Pizza("Margaritha", 60);
        Pizza2 p5 = new Pizza2("Margaritha", 60);

        Pizza17 p17a = new Pizza17("Margaritha", 60);
        Pizza17 p17b = new Pizza17("Margaritha", 60);
        Pizza17 p17c = new Pizza17(makeName(), 60);
        System.out.println("-".repeat(10));
        System.out.println(p17a.equals(p17b));
        System.out.println(p17b.equals(p17c));
        System.out.println(p17a);
        System.out.println("-".repeat(10));

    }
}
