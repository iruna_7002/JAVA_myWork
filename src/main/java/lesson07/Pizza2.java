package lesson07;

public class Pizza2 {
    String name;
    int size;

    public Pizza2(String name, int size){
        this.name = name;
        this.size = size;
    }
    @Override
    public String toString(){ return String.format("Pizza: %s of size %d", name, size);
    }

    @Override
    public  boolean equals(Object that0){
        if (that0 == null) return false;
        if (this == that0) return true;
        if (!(that0 instanceof Pizza)) return false; // two difference objects
        Pizza2  that = (Pizza2) that0;
        if (! this.name.equals(that.name)) return false; // Integer
        if (this.size != that.size) return false; // int
        return true;
    }
}
