package lesson07;

public class BoxExample1 {

    public static void main(String[] args) {

        Box box = new Box( new int[]{1, 2, 3});
        System.out.println(box);
        int[] data = box.data;
        data[2]  = -13;
        System.out.println(box);
        System.out.println("-".repeat(20));
    }
}
