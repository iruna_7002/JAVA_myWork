package lesson07;

public class Restaurant1 {

    public static String makeName(){
        return "Marga" + "RITA".toLowerCase();
    }
    public static void main(String[] args) {
        Pizza p1 = new Pizza(makeName(), 60);
        Pizza p2 = new Pizza(makeName(), 60);
        Pizza p3 = new Pizza("Margaritha", 60);
        Pizza p4 = new Pizza("Margaritha", 60);
        Pizza2 p5 = new Pizza2("Margaritha", 60);


        System.out.println(p1 == p2);
        System.out.println(p1.equals(p2));
        System.out.println(p1.equals(p5));
        System.out.println(p1);

    }
}
