package lesson07;

public class Restaurant17 {

    public static String makeName(){
        return "Marga" + "RITA".toLowerCase();
    }
    public static void main(String[] args) {


        Pizza17 p17a = new Pizza17("Margaritha", 60);
        Pizza17 p17b = new Pizza17("Margaritha", 60);
        Pizza17 p17c = new Pizza17(makeName(), 60);
        System.out.println("-".repeat(10));
        System.out.println(p17a.equals(p17b));
        System.out.println(p17b.equals(p17c));
        System.out.println(p17a);
        System.out.println("-".repeat(10));

    }
}
